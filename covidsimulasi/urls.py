from django.urls import path
from . import views

app_name = 'covidsimulasi'

urlpatterns = [
    path('', views.index, name='index'),
]