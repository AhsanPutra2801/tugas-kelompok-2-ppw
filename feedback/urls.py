from django.urls import path

from . import views

app_name = 'feedback'

urlpatterns = [
    path('', views.listfeedback, name='listfeedback'),
    path('tambah/', views.tambah, name='tambah'),
    path('saveform/',views.saveform, name='saveform'),
]
